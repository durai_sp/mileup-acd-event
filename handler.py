"""
handler.

Author: Durai S
email: dsankaran@agero.com
Date: Oct 09, 2017

handle functionality is used to initiate prod trip event.
"""

from dashboard import Dashboard
import collections


dd = [('latitude', 'float'),('tripId', 'string'), ('frameUTCTime', 'int'), ('isCrash', 'int'), ('type', 'string'), ('userId','string'), ('insertTime', 'int'), ('processedDateUtcTTL', 'int'), ('frameLocalTime','string'), ('deviceModel', 'string'), ('acdEventId', 'string'), ('longitude', 'float'), ('processedTimeUtc', 'int'), ('clientName', 'string'), ('acdClientEventId', 'string'), ('modifyLocalTime', 'string'), ('modifyTime', 'int'), ('crashConfirmed', 'string')]

dashboard = Dashboard('prod-acd-event', 'ppacn-agero-prod-acd-event', 'us-east-1', 'DEBUG')
dashboard.handler(dd)

