"""
LoadDataS3.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 12, 2017

LoadDataS3 class has the functionality to fetch data from DynamoDB and load it in S3
"""

from platformutils.dynamodb_utils import DynamoDBUtils
from platformutils.firehose_utils import FirehoseUtils
import json, ast
from decimal import Decimal
from platformutils.s3_utils import S3Utils
from platformutils.sns_utils import SNSUtils
import collections

class LoadDataS3(object):

    def __init__(self, stream_name, table_name, region_name, level):
        self.stream_name = stream_name
        self.table_name = table_name
        self.db = DynamoDBUtils(self.table_name, region_name, level)
        self.firehose = FirehoseUtils(self.stream_name, region_name, level)
        self.s3 = S3Utils(region_name, level)
        self.sns = SNSUtils(level)

    def default(self, obj):
        if isinstance(obj, Decimal):
            return str(obj)
        raise TypeError


    def get_data(self, table_meta_data, key, value, index_name=None):
        """
        This method is used to fetch data from DynamoDB.
        """
#        print table_meta_data
#       order_data = collections.OrderedDict(table_meta_data)        
        order_data = collections.OrderedDict(table_meta_data)
#        print order_data
        response = self.db.query_table(key, value, index_name)
        batch_size = 0
        count = 0
        records = []
        while True:
            for rows in response['Items']:
                row = json.dumps(rows, default=self.default)
                handled_d = self.handle_filter_data(order_data, row)
#                print handled_d               
                records.append(handled_d)
                batch_size = batch_size + 1
                count += 1
                if batch_size == 500:
                    self.put_record(records)
                    records = []
                    batch_size = 0
            if response.get('LastEvaluatedKey'):
                response = self.db.query_table(key, value, index_name, last_evaluated_key = response['LastEvaluatedKey'])
            else:
                break
        if batch_size > 0:
            self.put_record(records)
        self.slack_notification(count)


    def scan_data(self, table_meta_data):
        order_data = collections.OrderedDict(table_meta_data)
        table = self.db.get_table()
        batch_size = 0
        count = 0
        records = []
        response = table.scan()
        while True:
            for rows in response['Items']:
                row = json.dumps(rows, default=self.default)
                handled_d = self.handle_filter_data(order_data, row)
#                print handled_d
#                break
#            break
                records.append(handled_d)
                batch_size = batch_size + 1
                count += 1

                if batch_size == 500:
                    self.put_record(records)
                    records = []
                    batch_size = 0
            if response.get('LastEvaluatedKey'):
                response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
            else:
                break
        if batch_size > 0:
            self.put_record(records)

    def handle_filter_data(self, table_meta_data, datas):
        data = json.loads(datas)
        trip_data = ''
#        print table_meta_data
        for key, value in table_meta_data.items():
            t_data = data.get(key)
#            print t_data, key
            if t_data:
                if value == 'string':
                    if key == 'deviceModel':
                        a = t_data.split(",")
                        if len(a) == 2:
                            trip_data = trip_data + a[0] + "#" + a[1] + ","
                        else:
                            trip_data = trip_data + a[0] + ","
                    else:
                        trip_data = trip_data + t_data + ","
                elif value == 'int':
                    t_data = int(t_data)
                    trip_data = trip_data + `t_data` + ","
                elif value == 'float':
                    t_data = float(t_data)
                    trip_data = trip_data + `t_data` + ","
                elif value == 'boolean':
                    t_data = bool(t_data)
            else:
                trip_data = trip_data + ","

        return {'Data' : trip_data[:-1] + "\n"}

    def put_record(self, records):
        """
        This method is used to load records to Firehose Stream.
        :param records: Provide records has to be load.
        """
        self.firehose.put_records(records)

    def slack_notification(self, count):
        data = {}
        data['jobName'] = self.table_name
        data['totalRecord'] = count
        msg = json.dumps(data)
        arn = 'arn:aws:sns:us-east-1:951887592081:dynamo-redshift-alert'
        self.sns.publish_message(arn, msg)
