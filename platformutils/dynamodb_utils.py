"""
DynamoDB connection utils.

Author: Srinivas Rao Cheeti
email: scheeti@agero.com
Date: Mar 28, 2017

DynamoDBUtils class has the utils to query table, construct file with header and scan utils.
This could be used as a connector to DynamoDB and do the required tasks on the table.
"""

from time import sleep

import boto3
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError

import logging_helper as Log


class DynamoDBUtils(object):
    def __init__(self, table, r_name, level):
        self.logs = Log.log(level)
        self.logs.info('DynamoDB Utils initialized')
        self.dynamodb = boto3.resource('dynamodb', region_name=r_name)
        self.table = self.dynamodb.Table(table)
        self.RETRY_EXCEPTIONS = [
            "InternalServerError",
            "ProvisionedThroughputExceededException",
            "ThrottlingException"
        ]

    def query_table(self, dynamo_key, dynamo_value=None, index=None, last_evaluated_key=None, filter_key=None, filter_value=None):
        """This method is used to query DynamoDB table and return response item.

        :param dynamo_key: Provide the dynamodb key on which the query has to be performed.
        :param dynamo_value: Provide a conditional expression value if the query is with conditions.
        :param index: Provide the index name if the query is on index.
        :return Returns a dynmodb response item for the specific query.
        """
	if index and dynamo_value and last_evaluated_key and filter_key and filter_value:
	    self.logs.info('Querying on the index with Key and Value')
            query_kw = {'KeyConditionExpression': Key(dynamo_key).eq(dynamo_value), 'FilterExpression': Key(filter_key).eq(filter_value), 'IndexName': index, 'ExclusiveStartKey': last_evaluated_key}
	elif index and dynamo_value and filter_key and filter_value:
	    self.logs.info('Querying on the index with Key and Value')
            query_kw = {'KeyConditionExpression': Key(dynamo_key).eq(dynamo_value), 'FilterExpression': Key(filter_key).eq(filter_value), 'IndexName': index}
	elif index and dynamo_value and last_evaluated_key:
	    self.logs.info('Querying on the index with Key and Value')
            query_kw = {'KeyConditionExpression': Key(dynamo_key).eq(dynamo_value), 'IndexName': index, 'ExclusiveStartKey': last_evaluated_key}
        elif index and dynamo_value:
            self.logs.info('Querying on the index with Key and Value')
            query_kw = {'KeyConditionExpression': Key(dynamo_key).eq(dynamo_value), 'IndexName': index}
        elif not index and not dynamo_value:
            self.logs.info('Querying on the primary key with value')
            query_kw = {'KeyConditionExpression': Key(dynamo_key)}
	elif not index and not dynamo_value and last_evaluated_key:
            self.logs.info('Querying on the primary key with value')
            query_kw = {'KeyConditionExpression': Key(dynamo_key), 'ExclusiveStartKey': last_evaluated_key}
        elif index and not dynamo_value:
            self.logs.info('Querying on the index with key')
            query_kw = {'KeyConditionExpression': Key(dynamo_key), 'IndexName': index}
	elif index and not dynamo_value and last_evaluated_key:
            self.logs.info('Querying on the index with key')
            query_kw = {'KeyConditionExpression': Key(dynamo_key), 'IndexName': index, 'ExclusiveStartKey': last_evaluated_key}
	elif last_evaluated_key:
	    self.logs.info('Querying on the primary key with value')
            query_kw = {'KeyConditionExpression': Key(dynamo_key).eq(dynamo_value), 'ExclusiveStartKey': last_evaluated_key}
        else:
            self.logs.info('Querying on the primary key with value')
            query_kw = {'KeyConditionExpression': Key(dynamo_key).eq(dynamo_value)}

        response_object = self.table.query(**query_kw)
        return response_object

    def scan_table(self, dynamo_key, dynamo_value, retry_interval, max_attempts, index=None):
        """ To be done"""
        if index:
            self.logs.info('Scanning on the index')
            scan_kw = {'FilterExpression': Attr(dynamo_key).eq(dynamo_value), 'IndexName': index}
        else:
            self.logs.info('Scanning on the primary key')
            scan_kw = {'FilterExpression': Attr(dynamo_key).eq(dynamo_value)}
        retry_count = 0
        while True:
            try:
                response_object = self.table.scan(**scan_kw)
                last_key = response_object.get('LastEvaluatedKey')
                if not last_key:
                    raise ValueError('Key not fetched so retrying')
                else:
                    self.logs.info('Scan operation is available as the provisioned throughput is available')
                    retry_count = 0  # if successful, reset count
                    scan_kw.update({'ExclusiveStartKey': last_key})
            except (ClientError, ValueError) as err:
                print retry_count
                if retry_count < max_attempts:
                    if err.response['Error']['Code'] not in self.RETRY_EXCEPTIONS:
                        raise
                    self.logs.error('Retrying scan because {}'.format(err))
                    sleep(retry_interval * retry_count)
                    retry_count += 1
                    self.logs.info('Counter incremented and new value is {}'.format(retry_count))
                else:
                    self.logs.error('Retried scanning more than the default limit of 4')

                    # return response_object

    def insert_item_to_dynamo(self, item):
        """This method is used to insert a single item into DynamoDB table.

        :param item: Sample item to construct  {
                                                'column1' : 'value1', -- string
                                                'column2' : 'value2', -- string
                                                'column3' : value3 -- int
                                               }
        """
        self.table.put_item(
            Item=item
        )

    def update_item_in_dynamo(self, dynamo_key, dynamo_value, update_expression, exp_attr_values, exp_attr_names=None):
        """This method is used to update a item in dynamodb.

        :param dynamo_key: Pass the key, be it userId or tripId etc.
        :param dynamo_value: Pass the value for which the query is being done. Pass it as a string 'xyz' or a plain number if it is a integer.
        :param update_expression: Expression is required when updating set of columns for a particular key (say, key = {'tripid' : ASADSA-SDSDD}) and
                the update expression for this key would be something like this - "set #c = :countValue, #ml=:modifyLocalTime, #m=:modifyTime, #l=:lastSentTime"
        :param exp_attr_names: Expression attributes for the above update_expression would be like this - {
                                                                                                           '#d': 'countValue',
                                                                                                           '#ml': 'modifyLocalTime',
                                                                                                           '#m': 'modifyTime',
                                                                                                           '#l': 'lastSentTime'
                                                                                                          }
        :param exp_attr_values: Expression attributes for the above update_expression would be like this - {
                                                                                                            ':countValue': 1,
                                                                                                            ':modifyLocalTime': '2017-01-28',
                                                                                                            ':modifyTime': 123123243242343,
                                                                                                            ':lastSentTime': 123123123123123
                                                                                                           }
        :return This method returns a success or failure message for the update.
        """
        response = self.table.update_item(
            Key={
                dynamo_key: dynamo_value
            },
            UpdateExpression=update_expression,
            ExpressionAttributeValues=exp_attr_values,
            ExpressionAttributeNames=exp_attr_names,
            ReturnValues="UPDATED_NEW"
        )
        return response


    def query_trip_table_to_construct_file(self, file_path, array_of_columns, dynamo_key, dynamo_value, index):
        """This method is used to query a item in dynamodb and construct a file with the header.

        :param file_path: Pass the local file path to save the processed records ('/Users/xyz/Desktop/abc.txt')
        :param array_of_columns: Pass a array of required columns to query (array_of_columns = ['column1', 'column2', 'column3'])
        :param dynamo_key: Pass the key, be it userId or tripId etc.
        :param dynamo_value: Pass the value for which the query is being done. Pass it as a string 'xyz' or a plain number if it is a integer.
        :param index: If the query is performed on index, pass the index name ('myIndex-index'), else pass None (index=None)
        """
        header_value = ''
        for header_element in array_of_columns:
            header_value += str(header_element) + ','
        with open(file_path, "a") as construct_file:
            construct_file.write(header_value[:-1] + '\n')
        response_object = self.query_table(dynamo_key, dynamo_value, index)
        for item in response_object['Items']:
            item_write = ''
            for each_column in array_of_columns:
                if (each_column in item):
                    element_value = item[each_column]
                    item_write += str(element_value) + ','
                else:
                    item_write += str('') + ','
            with open(file_path, "a") as construct_file:
                construct_file.write(item_write[:-1] + '\n')
