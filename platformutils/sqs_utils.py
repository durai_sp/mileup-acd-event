"""
SQS connection utils.

Author: Srinivas Rao Cheeti
email: scheeti@agero.com
Date: April 5, 2017

SQSUtils class has the necessary functions to send, read and delete messages.
"""

import boto3
import logging_helper as Log


class SQSUtils():
    def __init__(self, level):
        self.logs = Log.log(level)
        self.logs.debug('SQS Utils initialized')
        self.sqs_client = boto3.client('sqs')

    def _get_queue_url(self, queue_name):
        """This method is used to fetch the sqs queue url from the queue name.

        :param queue_name: Provide the queue name.
        :return This method returns a the queue url.
        """
        self.logs.debug('Getting the queue url')
        url = self.sqs_client.get_queue_url(QueueName=queue_name)
        return url['QueueUrl']

    def send_message(self, body, attribute_name, json_object, queue_name):
        """This method is used to fetch the sqs queue url from the queue name.
        Sample 'attribute_name: json_object
                attribute_name = 'Author'
                json_object = {
                    'StringValue': 'Daniel',
                    'DataType': 'String'
                }
        :param body: Provide the body to be included in the messagebody name.
        :param attribute_name: Provide the attribute name to later read the message from the queue.
        :param json_object: Provide the constructed json object with string value and datatype .
        :param queue_name: Provide the queue name.
        :return This method returns the success or failure response.
        """
        self.logs.debug('Sending message for attribute - {:} to queue {:}'.format(attribute_name, queue_name))
        url = self._get_queue_url(queue_name)

        response = self.sqs_client.send_message(
            QueueUrl=url,
            MessageBody=body,
            DelaySeconds=5,
            MessageAttributes={attribute_name: json_object}
        )
        return response

    def read_message(self, attribute_name, queue_name):
        """This method is used to read messages from the sqs queue.

        :param attribute_name: Provide the attribute name to read the message from the queue.
        :param queue_name: Provide the queue name.
        :return This method returns the success or failure response on reading the message for a specific attribute.
        """
        self.logs.debug('Fetching message for attribute - {:} from queue {:}'.format(attribute_name, queue_name))
        url = self._get_queue_url(queue_name)
        response = self.sqs_client.receive_message(
            QueueUrl=url,
            AttributeNames=['All'],
            MessageAttributeNames=[
                attribute_name,
            ],
            VisibilityTimeout=60
        )
        return response

    def delete_message(self, handle, queue_name):
        """This method is used to delete the message from the queue on successfull processing the message.

        :param handle: Provide the handle which is returned in the read_message method to perform the deletion.
        :param queue_name: Provide the queue name.
        :return This method returns the success or failure response on reading the message for a specific attribute.
        """
        self.logs.debug('Deleting the handle {:}'.format(handle))
        response = self.sqs_client.delete_message(
            QueueUrl=self._get_queue_url(queue_name),
            ReceiptHandle=handle
        )
        return response
