"""
Firehose connection utils.

Author: Durai S
email: dsankaran@agero.com
Date: Jun 29, 2017

FirehoseUtils class has the utils to copying single and multiple records to redshift.

"""
import boto3
import logging_helper as Log
import time

class FirehoseUtils(object):

    def __init__(self, stream_name, r_name, level):
        self.logs = Log.log(level)
        self.logs.info('Firehose Utils initialized')
        self.client = boto3.client('firehose', region_name=r_name)
	self.delivery_stream = stream_name

    def put_record(self, data):
	"""
	:param data: Provide the data which is in dictionary format for loading into Redshift.
	"""
	start = time.clock()
	response = self.client.put_record(DeliveryStreamName=self.delivery_stream, Record= data)
     	self.logs.debug('Time taken to process - {:} is {:}'.format(str(len(data)),str(time.clock() - start)))

    def put_records(self, datas):
	"""
	:param datas: Provide the datas which is in list of dictionary format for loading into Redshift.
	"""
	start = time.clock()
	response = self.client.put_record_batch(DeliveryStreamName=self.delivery_stream, Records= datas)
      	self.logs.debug('Time taken to process - {:} is {:}'.format(str(len(datas)),str(time.clock() - start)))

